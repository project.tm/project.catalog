<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class project_catalog extends CModule {

    public $MODULE_ID = 'project.catalog';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_CATALOG_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_CATALOG_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_CATALOG_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        $eventManager = EventManager::getInstance();
        Loader::includeModule($this->MODULE_ID);

        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('catalog', 'OnProductAdd', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnProductUpdate');
        $eventManager->registerEventHandler('catalog', 'OnProductUpdate', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnProductUpdate');

        $eventManager->registerEventHandler('catalog', 'OnPriceAdd', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnPriceUpdate');
        $eventManager->registerEventHandler('catalog', 'OnPriceUpdate', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnPriceUpdate');

        $eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnBeforeIBlockElementDelete');
        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementDelete', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnAfterIBlockElementDelete');
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);

        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('catalog', 'OnProductAdd', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnProductUpdate');
        $eventManager->unRegisterEventHandler('catalog', 'OnProductUpdate', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnProductUpdate');

        $eventManager->unRegisterEventHandler('catalog', 'OnPriceAdd', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnPriceUpdate');
        $eventManager->unRegisterEventHandler('catalog', 'OnPriceUpdate', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnPriceUpdate');

        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnBeforeIBlockElementDelete');
        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementDelete', $this->MODULE_ID, '\Project\Catalog\Event\Catalog', 'OnAfterIBlockElementDelete');

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    protected function GetConnection() {
        return Application::getInstance()->getConnection(FavoritesTable::getConnectionName());
    }

}
